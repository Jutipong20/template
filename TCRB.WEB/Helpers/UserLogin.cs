﻿using System.Linq;
using System.Security.Claims;
using System.Web;
using TCRB.HELPER.Models.UserLogin;
using Utf8Json;
using static TCRB.Models.HELPERs.Authentication.AuthenticationModel;

namespace TCRB.WEB
{
    public static class UserLogin
    {
        public static UserLoginModel User()
        {
            var userCurrent = HttpContext.Current.User;
            var userJson = userCurrent.Claims.Where(u => u.Type == ClaimTypes.Sid).Select(r => r.Value).FirstOrDefault();
            var userdatajson = userCurrent.Claims.Where(u => u.Type == ClaimTypes.UserData).Select(r => r.Value).FirstOrDefault();
            var user = JsonSerializer.Deserialize<UserProfileModel>(userdatajson);
            return new UserLoginModel { Code = userJson.ToString() };
        }

    }
}
