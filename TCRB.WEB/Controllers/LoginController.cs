﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using TCRB.Models.HELPERs.AppSetting;
using TCRB.Models.HELPERs.Shared;
using static TCRB.Models.HELPERs.Authentication.AuthenticationModel;

namespace TCRB.WEB.Controllers
{
    public class LoginController : Controller
    {
        private readonly AppsittingModel _appsitting;

        public LoginController(IOptions<AppsittingModel> appsitting)
        {
            _appsitting = appsitting.Value;
        }

        [HttpGet]
        public IActionResult Index()
        {

            //return HttpContext.User.Identity.IsAuthenticated ? RedirectToAction("Index", "Home") : (IActionResult)View();
            return (IActionResult)View();

        }

        [HttpPost]
        public async Task<JsonResult> IndexAsync(UserProfileModel model)
        {
            ResponseModel result = new ResponseModel();
            //var obj = (ClaimResponseModel)result.data;
            //var identity = new ClaimsIdentity(new[] {
            //        new Claim(ClaimTypes.Sid, Guid.NewGuid().ToString()),
            //        new Claim(ClaimTypes.UserData, JsonSerializer.Serialize(model))
            //});
            //HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity), new AuthenticationProperties
            //{
            //    ExpiresUtc = DateTime.UtcNow.AddMinutes(2),
            //});

            var claims = new List<Claim> {
                new Claim(ClaimTypes.UserData, JsonSerializer.Serialize(model)),
                new Claim(ClaimTypes.Sid, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, "Administrator")
            };
            ClaimsIdentity userIdentity = new ClaimsIdentity(claims, "login");
            ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);

            await HttpContext.SignInAsync(principal, new AuthenticationProperties
            {
                IsPersistent = false,
                ExpiresUtc = DateTime.UtcNow.AddMinutes(_appsitting.LoginTimeExpired)
            });

            result.Success = true;
            return Json(result);
        }
        [HttpGet]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            HttpContext.Response.Cookies.Delete(".AspNetCore.Cookies");
            return RedirectToAction("Index", "Login");
        }

    }
}