﻿using System;

namespace TCRB.DAL.EntityModel
{
    public partial class User
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserTname { get; set; }
        public string Email { get; set; }
        public string ActiveFlag { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}
