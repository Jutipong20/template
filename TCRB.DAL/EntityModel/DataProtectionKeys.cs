﻿namespace TCRB.DAL.EntityModel
{
    public partial class DataProtectionKeys
    {
        public int Id { get; set; }
        public string FriendlyName { get; set; }
        public string Xml { get; set; }
    }
}
