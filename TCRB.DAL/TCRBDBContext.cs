﻿using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using TCRB.DAL.EntityModel;
using TCRB.Models.HELPERs.AppSetting;

namespace TCRB.DAL.TCRBContext
{
    public class TCRBDBContext : TCRBContext, IDataProtectionKeyContext
    {
        private readonly AppsittingModel _configuration;

        public TCRBDBContext(IOptions<AppsittingModel> configuration)
        {
            _configuration = configuration.Value;
        }

        public TCRBDBContext(DbContextOptions<TCRBContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_configuration.ConnectionStrings.TCRBDB);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<DataProtectionKeys>();
            modelBuilder.Entity<DataProtectionKey>().ToTable("DataProtectionKeys");
            base.OnModelCreating(modelBuilder);
        }
        public new DbSet<DataProtectionKey> DataProtectionKeys { get; set; }
    }
}
