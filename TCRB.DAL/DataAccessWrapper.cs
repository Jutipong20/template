﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Threading.Tasks;
using TCRB.DAL.TCRBContext;
using TCRB.Models.HELPERs.AppSetting;

namespace TCRB.DAL
{
    public class DataAccessWrapper : IDataAccessWrapper
    {
        private readonly TCRBDBContext _context;
        private readonly IOptions<AppsittingModel> _config;
        private readonly ILoggerFactory _loggerFactory;

        //private IChanelDataAccess _chanelAccess;

        public DataAccessWrapper(TCRBDBContext context, IOptions<AppsittingModel> config, ILoggerFactory loggerFactory)
        {
            _context = context;
            _config = config;
            _loggerFactory = loggerFactory;
        }

        //public IChanelDataAccess ChanelDataAccess => _chanelAccess ?? (_chanelAccess = new ChanelDataAccess(_context, _config, _loggerFactory));

        public void SaveChanges() => _context.SaveChanges();
        public async Task SaveChangesAsync() => await _context.SaveChangesAsync();
        public int Count<T>(IQueryable<T> query)
        {
            return query.Count();
        }
    }
}
