﻿using System.Linq;
using System.Threading.Tasks;

namespace TCRB.DAL
{
    public interface IDataAccessWrapper
    {
        //IChanelDataAccess ChanelDataAccess { get; }

        void SaveChanges();
        Task SaveChangesAsync();
        int Count<T>(IQueryable<T> query);
    }
}
