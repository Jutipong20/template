﻿using System.Collections.Generic;
using TCRB.HELPER.Models.Shared;

namespace TCRB.HELPER.Models.Home
{
    public class Demo01ViewModel
    {
        public List<Select2Model> Master_Select2Single { get; set; }
        public List<Select2Model> Master_Select2Multiple { get; set; }
    }
}
