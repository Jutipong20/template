﻿namespace TCRB.HELPER.Models.Shared
{
    public class Select2Model
    {
        public string id { get; set; }
        public string text { get; set; }
        public bool selected { get; set; }
    }

    public class Select2SearchModel
    {
        public string Search { get; set; }
        public int Limit { get; set; }

        public Select2SearchModel()
        {
            this.Limit = 30;
        }
    }
}
