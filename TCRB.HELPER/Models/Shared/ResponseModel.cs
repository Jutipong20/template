﻿using Microsoft.AspNetCore.Http;
using TCRB.HELPER;

namespace TCRB.Models.HELPERs.Shared
{
    public class ResponseModel
    {
        public string ID { get; set; }
        public string Code { get; set; }

        public object Datas { get; set; }
        public int Total { get; set; } = 0;

        private bool _Success = false;
        public bool Success
        {
            get
            {
                return _Success;
            }
            set
            {
                _Success = value;
            }
        }

        private int _StatusCode = StatusCodes.Status500InternalServerError;
        public int StatusCode
        {
            get
            {
                return _Success ? StatusCodes.Status200OK : StatusCodes.Status500InternalServerError;
            }
            set
            {
                _StatusCode = value;
            }
        }

        private string _Message = EnumStatus.Fail.AsString();
        public string Message
        {
            get
            {
                return _Success ? EnumStatus.Success.AsString() : EnumStatus.Fail.AsString();
            }
            set
            {
                _Message = value;
            }
        }
    }
}
