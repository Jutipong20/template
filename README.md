======================================= Create User =======================================

USE [TCRBDB]
GO

/****** Object:  Table [dbo].[User]    Script Date: 12/28/2019 2:52:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[User](
	[ID] [uniqueidentifier] NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[NameTH] [nvarchar](50) NOT NULL,
	[NameEN] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](50) NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateBy] [nvarchar](50) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

======================================= Scaffold =======================================


Scaffold-DbContext "Server=.\SQLEXPRESS;Database=TCRBDB;User Id=sa;Password=p@ssw0rd;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir EntityModel -Context TCRBContext -Force


